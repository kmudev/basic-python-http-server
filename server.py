from urllib.parse import urlparse

import datetime
import http.server
import json
import logging
import socket
import socketserver
import sys

import netifaces



class GetHandler(http.server.BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.0'

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()


    def do_HEAD(self):
        self._set_headers()


    def do_GET(self):
        logging.info('\n========= HEADERS =========\n' + 
                      str(self.headers) + 
                      '===========================')

        if self.path == '/time':
            self.do_time()
        elif self.path == '/health':
            self.do_health()
        elif self.path == '/hostname':
            self.do_hostname()
        elif self.path == '/interfaces':
            self.do_interfaces()
        elif self.path == '/':
            self.do_root()


    def do_root(self):
        parsed_path = urlparse(self.path)
        self._set_headers()
        self.wfile.write(json.dumps({
            'method': self.command,
            'path': self.path,
            'real_path': parsed_path.query,
            'query': parsed_path.query,
            'request_version': self.request_version,
            'protocol_version': self.protocol_version
        }).encode())


    def do_time(self):
        self._set_headers()
        self.wfile.write(json.dumps({'time': str(datetime.datetime.now())}).encode())


    def do_health(self):
        self._set_headers()
        self.wfile.write(json.dumps({'status': 'ok'}).encode())


    def do_hostname(self):
        self._set_headers()
        self.wfile.write(json.dumps({'hostname': str(socket.gethostbyaddr(socket.gethostname())[0])}).encode())


    def do_interfaces(self):
        self._set_headers()
        interfaces_dict = get_interfaces_addresses()
        self.wfile.write(json.dumps(interfaces_dict).encode())
        
    def do_HEAD(self):
        self._set_headers()


def get_interfaces_addresses():
    interfaces_list = netifaces.interfaces()
    interfaces_ips_dict = dict()

    for interface in interfaces_list:
        interfaces_ips_dict[interface] = dict()
        #AF_LINK is alias for AF_PACKET
        if netifaces.AF_LINK in netifaces.ifaddresses(interface).keys():
            interfaces_ips_dict[interface]['mac'] = netifaces.ifaddresses(interface)[netifaces.AF_LINK][0]
        if netifaces.AF_INET in netifaces.ifaddresses(interface).keys():
            interfaces_ips_dict[interface]['ip'] = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]

    return interfaces_ips_dict


def setup_logging():
    file_handler = logging.FileHandler(filename='server.log')
    stdout_handler = logging.StreamHandler(sys.stdout)
    handlers = [file_handler, stdout_handler]

    logging.basicConfig(
            level=logging.DEBUG,
            format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
            handlers=handlers)

    logger = logging.getLogger('LOGGER_NAME')


def main():
    setup_logging()
    
    PORT = 9000
    Handler = GetHandler
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print("Serving at port: ", PORT)
        httpd.serve_forever()


if __name__ == '__main__':
    main()